from reversion.admin import VersionAdmin

from django.contrib import admin

from pet_back.list.models import List, Task


class TaskInLine(admin.TabularInline):
    model = Task
    extra = 1
    fields = ['title', ]



class ListAdmin(VersionAdmin):
    inlines = TaskInLine,


class TaskAdmin(VersionAdmin):
    list_display = ['title',]




admin.site.register(List, ListAdmin)
admin.site.register(Task, TaskAdmin)
