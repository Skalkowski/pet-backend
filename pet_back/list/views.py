from rest_framework import viewsets

from pet_back.list.models import List
from pet_back.api.serializers import ListSerializer


class ListViewSet(viewsets.ModelViewSet):
    queryset = List.objects.all()
    serializer_class = ListSerializer
