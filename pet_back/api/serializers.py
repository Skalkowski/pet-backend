from rest_framework import serializers

from pet_back.list.models import List, Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task


class ListSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, required=False)

    class Meta:
        model = List
        fields = ('pk', 'name', 'tasks')

