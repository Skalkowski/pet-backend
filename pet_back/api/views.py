from rest_framework import viewsets

from pet_back.list.models import List, Task
from pet_back.api.serializers import ListSerializer, TaskSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = List.objects.all()
    serializer_class = ListSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
