from django.conf.urls import url

from pet_back.api.views import TaskViewSet
from pet_back.list.views import ListViewSet

urlpatterns = [
                       url(r'list/(?P<pk>\d+)?$', ListViewSet.as_view(
                           actions={'get': 'list', 'delete': 'destroy', 'put': 'update', 'post': 'create'}),
                           name='urls_list'),
                       url(r'task/(?P<pk>\d+)?$', TaskViewSet.as_view(
                           actions={'get': 'list', 'delete': 'destroy', 'put': 'update', 'post': 'create'}),
                           name='urls_task'),
                       ]
